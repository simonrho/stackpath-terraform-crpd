output "crpd-workload-instances" {
  value = {
  for instance in stackpath_compute_workload.crpd.instances :
  instance.name => {
    name = instance.name
    public_ip = instance.external_ip_address
    private_ip = instance.ip_address
    phase = instance.phase
  }}
}
