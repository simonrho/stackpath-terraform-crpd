# Optionally you can use following environmental variables instead of hardcoded the sensitive information in the code
# Environment variables
#   STACKPATH_CLIENT_ID
#   STACKPATH_CLIENT_SECRET
#   STACKPATH_STACK_ID

stackpath_stack_id = "<your stack id (SLUG)>"
stackpath_client_id = "<your API client id>"
stackpath_client_secret = "<your API client secret>"

# container image repository information
container_image_name = "hub.juniper.net/routing/crpd:19.2R1.8"
container_registry_username = "<your username for access to the juniper container registry>"
container_registry_password = "<your password for access to the juniper container registry>"
container_registry_server = "hub.juniper.net"
