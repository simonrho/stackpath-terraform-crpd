variable "stackpath_stack_id" {}
variable "stackpath_client_id" {}
variable "stackpath_client_secret" {}

variable "container_image_name" {}
variable "container_registry_username" {}
variable "container_registry_password" {}
variable "container_registry_server" {}

