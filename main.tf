terraform {
  required_providers {
    stackpath = {
      source = "stackpath/stackpath"
      version = "1.3.3"
    }
  }
}

provider "stackpath" {
  stack_id = var.stackpath_stack_id
  client_id = var.stackpath_client_id
  client_secret = var.stackpath_client_secret
}

locals {
  slug = "crpd-20"
  command = [
      "/bin/bash",
      "-c",
      join(" && ", [
        "/bin/echo ${filebase64("./juniper.conf")} | /usr/bin/base64 --decode > /juniper.conf",
        "/bin/echo ${filebase64("./launch.sh")} | /usr/bin/base64 --decode > /launch.sh && chmod +x /launch.sh && /launch.sh",
        "/sbin/runit-init 0"
      ])
    ]
}

resource "stackpath_compute_workload" "crpd" {
  name = local.slug
  slug = local.slug

  labels = {
    "role" = "routing"
    "environment" = "trial"
  }

  network_interface {
    network = "default"
  }

  image_pull_credentials {
    docker_registry {
      username = var.container_registry_username
      password = var.container_registry_password
      server = var.container_registry_server
    }
  }

  container {
    name = local.slug
    image = var.container_image_name

    command = local.command

    resources {
      requests = {
        "cpu" = "1"
        "memory" = "2Gi"
      }
    }
  }

  target {
    name = "us"
    deployment_scope = "cityCode"
    min_replicas = 1
    selector {
      key = "cityCode"
      operator = "in"
      values = [
        "JFK"
      ]
    }
  }
}

resource "stackpath_compute_network_policy" "crpd-network-policy" {
  name = "Network Policy for ${stackpath_compute_workload.crpd.id}"
  slug = "workload-${stackpath_compute_workload.crpd.id}"
  description = ""

  instance_selector {
    key = "workload.platform.stackpath.net/workload-id"
    operator = "in"
    values = [
      stackpath_compute_workload.crpd.id
    ]
  }

  policy_types = [
    "INGRESS"
  ]

  priority = 1000

  ingress {
    description = "ssh"
    action = "ALLOW"
    from {
      ip_block {
        cidr = "173.76.106.0/24"
      }
    }
    protocol {
      tcp {
        destination_ports = [
          22
        ]
      }
    }
  }

  ingress {
    description = "netconf"
    action = "ALLOW"
    from {
      ip_block {
        cidr = "173.76.106.0/24"
      }
    }
    protocol {
      tcp {
        destination_ports = [
          830
        ]
      }
    }
  }
}
