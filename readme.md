# Terrafrom code for Juniper CRPD StackPath deployment
This terraform code is to create/update/delete the CRPD (Containerized routing daemon) instance in the StackPath Edge Compute sites.

## Supported features
* Initial CRPD JUNOS configuration commit
* Enable SSH and NETCONF service
* Auto-create network security policy
* Direct pull CRPD image from Juniper container image repository without building custom RPD image from a pristine CRPD image 

## Running steps
#### terrafrom init

```bash
srho-mbp:crpd srho$ terraform init

Initializing the backend...

Initializing provider plugins...
- Reusing previous version of stackpath/stackpath from the dependency lock file
- Installing stackpath/stackpath v1.3.3...
- Installed stackpath/stackpath v1.3.3 (self-signed, key ID 8346F13F182169D6)

Partner and community providers are signed by their developers.
If you'd like to know more about provider signing, you can read about it here:
https://www.terraform.io/docs/plugins/signing.html

Terraform has been successfully initialized!

You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.

If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.
```

#### terrafrom apply
```bash
srho-mbp:crpd srho$ terraform apply 

An execution plan has been generated and is shown below.
Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # stackpath_compute_network_policy.crpd-network-policy will be created
  + resource "stackpath_compute_network_policy" "crpd-network-policy" {
      + id           = (known after apply)
      + name         = (known after apply)
      + policy_types = [
          + "INGRESS",
        ]
      + priority     = 1000
      + slug         = (known after apply)
      + version      = (known after apply)

      + ingress {
          + action      = "ALLOW"
          + description = "ssh"

          + from {

              + ip_block {
                  + cidr = "173.76.106.0/24"
                }
            }

          + protocol {

              + tcp {
                  + destination_ports = [
                      + "22",
                    ]
                }
            }
        }
      + ingress {
          + action      = "ALLOW"
          + description = "netconf"

          + from {

              + ip_block {
                  + cidr = "173.76.106.0/24"
                }
            }

          + protocol {

              + tcp {
                  + destination_ports = [
                      + "830",
                    ]
                }
            }
        }

      + instance_selector {
          + key      = "workload.platform.stackpath.net/workload-id"
          + operator = "in"
          + values   = (known after apply)
        }
    }

  # stackpath_compute_workload.crpd will be created
  + resource "stackpath_compute_workload" "crpd" {
      + annotations = (known after apply)
      + id          = (known after apply)
      + labels      = {
          + "environment" = "trial"
          + "role"        = "routing"
        }
      + name        = "crpd-20"
      + slug        = "crpd-20"

      + container {
          + command = [
              + "/bin/bash",
              + "-c",
              + "/bin/echo IyBQbGVhc2UgZG8gbm90IHBsYWNlIHNzaCBvciBuZXRjb25mIHNlcnZpY2UgY29uZmlndXJhdGlvbi4KIyBPdGhlcndpc2UsIHRoZSBjb21taXQgY29tbWFuZCB3aWxsIGJlIHVuc3VjY2Vzc2Z1bC4KIyBUaGUgc3NoIGFuZCBuZXRjb25mIHNlcnZpY2UgY29uZmlndXJhdGlvbiBpcyBpbiB0aGUgZmlsZSAtIC9ldGMvc3NoL3NzaGRfY29uZmlnCgpzeXN0ZW0gewogICAgcm9vdC1hdXRoZW50aWNhdGlvbiB7CiAgICAgICAgZW5jcnlwdGVkLXBhc3N3b3JkICIkNiQyNThPbiRVUzkzakxyUVB3cWpscGdQTTg0TjhmOW9zOVNMQjBCZ0Zyc2d2Y21BY1N3cnQ0NURFL0doV3BVUVRELnBWVWdpSmJZenhnT2tncnJIbmJ3WmZoRy5QLyI7ICMjIFNFQ1JFVC1EQVRBCiAgICAgICAgc3NoLXJzYSAic3NoLXJzYSBBQUFBQjNOemFDMXljMkVBQUFBREFRQUJBQUFCQVFEQlBhNWJHaisxZHlqSm9iZmNZeDhRbmNKRWx5eWtWWkd6MjNRTmhQQXNOWGg0dFpsNXk2V3dKOG9UV0h0RHpqeXlkemxndnB1anZWbnE1Yjg4S1BiaW90bDBEb0VzbHZhemhrM082ZGw0OHpvam1xL0lsTU1ISTNUMGFuMjVzb083d1NpU3dnMUQ2eFpGTDNiOXYyUGpzZWgzYXhqeDBQbnNobGxQdjlUdllsMTJqaXROUm5vYTFXYUZJR3VPRS9yWjVEbUFrUFNXQ2gwR1RsYzM1VmN4V1BxRGYvb293QkVQeEwxMlpodC8rQjIyako0WmtQb3hIK1pHbExVaGRPbXpxeHpBMjYvVjI0dHZRKzZHSEJXZm1yamVMVmsvSjlkcjdUQWplRkNvTHk1ZHhuYVgyeVFONGMwdnBSd1kvMXhKY3F3YjhVYlUzVElFWHBJQU55dVIgc3Job0BzcmhvLW1icDE1IjsgIyMgU0VDUkVULURBVEEKICAgIH0KICAgIGxpY2Vuc2UgewogICAgICAgIGtleXMgewogICAgICAgICAgICBrZXkgIjIwIDI0MyBOaSBMT05HIE5PUk1BTCBTVEFOREFMT05FIEFHR1IgMV9LRVlTIDFfS0VZUyAxIERFQyAyMDIwIDAgMCAxIE1BWSAyMDIxIDIzIDU5IE5pTCBTTE1fQ09ERSBERU1PIE5pTCBOaUwgTmkgTmlMIE5pTCAxNV9NSU5TIE5pTCAwIE5IZDR0aEduYWkxWVdlTjVJWFhLWG9NcmNYTDY4byt2bTlkMWd2NHh3eEJlbktUZFJITWRtOEg1cTJ0cldCUGJvTVFZM0d2TGZmQUtlVVYvdVlFYTFkTU84ZG1GbEY3SmhQblVhOWpCR3FxSkFBRFEyUUR3RjdXUnR5c1hMcC90NVhJdEswTE5DcWRHRjFBMFpGNlVGTnM1eGVJRStWcXkzNU1YSkI3em1wMm1RVU0vNEhkVGwwV1JxMm5CeFR0OFBxcDZTS0JlUkE2Lzh2bnVKYVMzTDRLT2dSK3czQUJoZkR5MHNBbzUwMUdTczZSSWtYNzc5dFRhdmV2M3dmV0xLY0dabTRNdGhXT1BOblMrc1Ara3U5d0dLWXFOVEc2cGxjK3NOT0ZIdXpQQys0NElJb3R5aThIV0djaGc3c0QrSG9kczBCb3dzakdTNk5XK3A1U3Y4WDdtKzYyZ0FsZVVRN0VxYmN3emlHSWxBKy9mNkRzOTI3b2dxOXBkNTFQL3RoWUozOVJ0SDdPT1Bmdk5uam1yRFNPZG9ObmNCdCtrUUJkQ2kxNmV3cEJjWWwxL2c1SGR6T2tRekhiVk9iNzdQSTBZMnJLRHE1cEFHQ3c2YjNGa0RhQ0Y4UFBNbHRvenBEWU1sd1FzS3JKRytOU3VNd0lvTVM3Qk9ITG9FUU1ya3V6N2MzWENzaVBqd1hTcmw4N056Z0xMZEVOWGJEVnVkcU1KU1hUSEQ2RHliY2lZQmJZVzBkR2JOb0MvWk41Q0JRUlNRMEl5QzRNSkJRUlNRMEl5QmdFQkJ3RUJCQ1U0WWpGaU5tWXhaQzB6TnpZNUxUUm1OR1V0WWpNNE55MHpOR1V6TW1WbE9UWXpZbVFBQ0FFQkNVQ2x5ZElwVVppNm44anpJTE9yVlBhZzU5WE5EQWl6cFljazY4Z2M0dUxmUklUekdtZFFUS2ZhOVUxSzhCR2p6ZmJYMFM0d0NEWmhtNGxoNTVlZnBMWFZDb0lPTUlJQkNnS0NBUUVBMk9NZ0VTUWgxZno0TGpYWjBTbURzclBHSjJjQjV6VTRDUHNRbVFqMEZqbmpVWWZGNDFuREdPNk80bXBJbnk1V1R6RkpZU3A2MTcxOW9aeURFVnNKWVp3bnFLM056eXN3TkJWajNDcVNIeDNIcEtDdTFucUFOMXNDNzloYnhhM0xzYnZHYTI1MjJqdE16aHJkN0YzTWR5Y0dld2EyTzA2MHJyQno5WnJveGlycUg2Wng0am16RlJKYmdaWDdVVU9Kc3dXM2I1Y1RLbE95WC9ZWWwxclFJb2FaK2YxWXRFYUlSSFE4ajlqOFhuL0c0QVQxWGpRRk9IOVlmbzM5UFBFUkdyMXNDR09JbEtwWVdaSmhQNkwzSHFJTUYyMnRzQVdKTFZrT0lQQ3VJL1BacGs1Vk1tWVpvRjNLTWRvNWtTZkFUZ290VzZ1ZmhxU05TYmhSNm51N2l3SURBUUFCQkNVNFlqRmlObVl4WkMwek56WTVMVFJtTkdVdFlqTTROeTB6TkdVek1tVmxPVFl6WW1RQUJ3RUJESUlBai8wR1hSU0c0UlpnY3ppcWRtTnVHQXJGSnN6WEEwMXZ4dUdUVVMxZFVqZGYwUEtCdDBycC85MkwwU29PUGNUVC9ldVZkYUZKSUNlY3FKaDVpcU1CQUV0TEF3MmZDZVZIV1NRT1FkeWozZGN0aEtoQlU5a3JoeWJkOU1RKzZac2kxVFVSZU9LcUxpVHV1bTdwNklEeVZIcUlJU0FmRWhvRTdqMkE0NDZtMUpmSk9OMDhMdWpFcm03YzJmOVBGWUkwRk1iam9jQlR1b3RIMmdxYWVtUlBHZHFhc1lFUDJhUHJPZGovYlFlR0VydytZMldVTHJrUFl4UXNpTER3VHpjbkVjekRtbVJIcTBoRHZDZENBczJiSjhxMENGZXpOWFdKUlNIQnFkNFpSV3ZmZzhUQ0N5S0ZyRlNLcUxsSCtTQWd6cVcrRDBOamY3a3F2ODdoaVZ6L1F3QUFBcjg9I0tleVR5cGU9Q29tbWVyY2lhbCNBSUQ9YzFjYzRkYTctMmQ1YS00MDIwLWJjYTAtNGY2OTQwYjZmMmFiIFNJR049MDcyODY2MkQxQ0YzRkVCODVFQTdGNDRFNTY5MjMxNzA5QjFFQzVGQzY1QTlCOTAxRDZGNDFDNjYwM0Y1RDkxMUJCOEM1OEY5NTVBQjQxREMzNjI0IjsKICAgICAgICB9CiAgICB9Cn0K | /usr/bin/base64 --decode > /juniper.conf && /bin/echo IyEvYmluL2Jhc2gKCmNhdCA8PEVPTCA+IC9ldGMvc3NoL3NzaGRfY29uZmlnClBvcnQgMjIKUGVybWl0Um9vdExvZ2luID0geWVzClBvcnQgODMwClN1YnN5c3RlbSBuZXRjb25mIC91c3IvYmluL25ldGNvbmYKTWF0Y2ggTG9jYWxQb3J0IDgzMApBbGxvd1RDUEZvcndhcmRpbmcgbm8KWDExRm9yd2FyZGluZyBubwpGb3JjZUNvbW1hbmQgbmV0Y29uZgpFT0wKCnNlcnZpY2Ugc3NoIHJlc3RhcnQKCmNhdCA8PEVPRiA+PiAvZXRjL2luaXQuZC9yY1MKIyBzZXQgbG9vcGJhY2sgaXAgYWRkcmVzc2VzLgppZiBbICEgLXogIiR7TE9PUEJBQ0tfQUREUkVTU30iIF07IHRoZW4KICBpcCAtNCBhZGRyIGFkZCAiJHtMT09QQkFDS19BRERSRVNTfS8zMiIgZGV2IGxvCmZpCgojIGNvbW1pdCBpbml0aWFsIGNvbmZpZ3VyYXRpb24uCi91c3Ivc2Jpbi9tZ2QgLWQgLVpTIC1aTyAtWmEgYXV0b2luc3RhbGwtbWVyZ2UgL2p1bmlwZXIuY29uZiB8fCB0cnVlCgpFT0YK | /usr/bin/base64 --decode > /launch.sh && chmod +x /launch.sh && /launch.sh && /sbin/runit-init 0",
            ]
          + image   = "hub.juniper.net/routing/crpd:19.2R1.8"
          + name    = "crpd-20"

          + resources {
              + requests = {
                  + "cpu"    = "1"
                  + "memory" = "2Gi"
                }
            }
        }

      + image_pull_credentials {
          + docker_registry {
              + password = (sensitive value)
              + server   = "hub.juniper.net"
              + username = "JNPR-FieldUser12"
            }
        }

      + instances {
          + external_ip_address = (known after apply)
          + ip_address          = (known after apply)
          + message             = (known after apply)
          + name                = (known after apply)
          + phase               = (known after apply)
          + reason              = (known after apply)

          + container {
              + command = (known after apply)
              + image   = (known after apply)
              + name    = (known after apply)

              + env {
                  + key          = (known after apply)
                  + secret_value = (sensitive value)
                  + value        = (known after apply)
                }

              + liveness_probe {
                  + failure_threshold     = (known after apply)
                  + initial_delay_seconds = (known after apply)
                  + period_seconds        = (known after apply)
                  + success_threshold     = (known after apply)
                  + timeout_seconds       = (known after apply)

                  + http_get {
                      + http_headers = (known after apply)
                      + path         = (known after apply)
                      + port         = (known after apply)
                      + scheme       = (known after apply)
                    }

                  + tcp_socket {
                      + port = (known after apply)
                    }
                }

              + port {
                  + enable_implicit_network_policy = (known after apply)
                  + name                           = (known after apply)
                  + port                           = (known after apply)
                  + protocol                       = (known after apply)
                }

              + readiness_probe {
                  + failure_threshold     = (known after apply)
                  + initial_delay_seconds = (known after apply)
                  + period_seconds        = (known after apply)
                  + success_threshold     = (known after apply)
                  + timeout_seconds       = (known after apply)

                  + http_get {
                      + http_headers = (known after apply)
                      + path         = (known after apply)
                      + port         = (known after apply)
                      + scheme       = (known after apply)
                    }

                  + tcp_socket {
                      + port = (known after apply)
                    }
                }

              + resources {
                  + requests = (known after apply)
                }

              + volume_mount {
                  + mount_path = (known after apply)
                  + slug       = (known after apply)
                }
            }

          + location {
              + city             = (known after apply)
              + city_code        = (known after apply)
              + continent        = (known after apply)
              + continent_code   = (known after apply)
              + country          = (known after apply)
              + country_code     = (known after apply)
              + latitude         = (known after apply)
              + longitude        = (known after apply)
              + name             = (known after apply)
              + region           = (known after apply)
              + region_code      = (known after apply)
              + subdivision      = (known after apply)
              + subdivision_code = (known after apply)
            }

          + metadata {
              + annotations = (known after apply)
              + labels      = (known after apply)
            }

          + network_interface {
              + gateway            = (known after apply)
              + ip_address         = (known after apply)
              + ip_address_aliases = (known after apply)
              + network            = (known after apply)
            }

          + virtual_machine {
              + image     = (known after apply)
              + name      = (known after apply)
              + user_data = (known after apply)

              + liveness_probe {
                  + failure_threshold     = (known after apply)
                  + initial_delay_seconds = (known after apply)
                  + period_seconds        = (known after apply)
                  + success_threshold     = (known after apply)
                  + timeout_seconds       = (known after apply)

                  + http_get {
                      + http_headers = (known after apply)
                      + path         = (known after apply)
                      + port         = (known after apply)
                      + scheme       = (known after apply)
                    }

                  + tcp_socket {
                      + port = (known after apply)
                    }
                }

              + port {
                  + enable_implicit_network_policy = (known after apply)
                  + name                           = (known after apply)
                  + port                           = (known after apply)
                  + protocol                       = (known after apply)
                }

              + readiness_probe {
                  + failure_threshold     = (known after apply)
                  + initial_delay_seconds = (known after apply)
                  + period_seconds        = (known after apply)
                  + success_threshold     = (known after apply)
                  + timeout_seconds       = (known after apply)

                  + http_get {
                      + http_headers = (known after apply)
                      + path         = (known after apply)
                      + port         = (known after apply)
                      + scheme       = (known after apply)
                    }

                  + tcp_socket {
                      + port = (known after apply)
                    }
                }

              + resources {
                  + requests = (known after apply)
                }

              + volume_mount {
                  + mount_path = (known after apply)
                  + slug       = (known after apply)
                }
            }
        }

      + network_interface {
          + network = "default"
        }

      + target {
          + deployment_scope = "cityCode"
          + min_replicas     = 1
          + name             = "us"

          + selector {
              + key      = "cityCode"
              + operator = "in"
              + values   = [
                  + "JFK",
                ]
            }
        }
    }

Plan: 2 to add, 0 to change, 0 to destroy.

Changes to Outputs:
  + crpd-workload-instances = (known after apply)

Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value: yes

stackpath_compute_workload.crpd: Creating...
stackpath_compute_workload.crpd: Creation complete after 1s [id=43aa4810-5500-4ba5-8e2c-a65119f211d6]
stackpath_compute_network_policy.crpd-network-policy: Creating...
stackpath_compute_network_policy.crpd-network-policy: Creation complete after 0s [id=2fe73e85-25d1-4af2-b53a-028c629d21ee]

Apply complete! Resources: 2 added, 0 changed, 0 destroyed.

Outputs:

crpd-workload-instances = {}

```


#### 2nd terraform apply
The StackPath service takes some time to create instance(s) and the internal and external address information will be available tens of seconds later.

```bash
srho-mbp:crpd srho$ terraform apply --auto-approve
stackpath_compute_workload.crpd: Refreshing state... [id=43aa4810-5500-4ba5-8e2c-a65119f211d6]
stackpath_compute_network_policy.crpd-network-policy: Refreshing state... [id=2fe73e85-25d1-4af2-b53a-028c629d21ee]
stackpath_compute_workload.crpd: Modifying... [id=43aa4810-5500-4ba5-8e2c-a65119f211d6]
stackpath_compute_workload.crpd: Modifications complete after 1s [id=43aa4810-5500-4ba5-8e2c-a65119f211d6]

Apply complete! Resources: 0 added, 1 changed, 0 destroyed.

Outputs:

crpd-workload-instances = {
  "crpd-20-us-jfk-0" = {
    "name" = "crpd-20-us-jfk-0"
    "phase" = "RUNNING"
    "private_ip" = "10.128.0.2"
    "public_ip" = "151.139.31.140"
  }
}


```

#### ssh access to the CRPD instance
```bash
srho-mbp:crpd srho$ ssh root@151.139.31.140
Warning: Permanently added '151.139.31.140' (ECDSA) to the list of known hosts.

===>
           Containerized Routing Protocols Daemon (CRPD)
 Copyright (C) 2018-19, Juniper Networks, Inc. All rights reserved.
                                                                    <===

root@crpd-20-us-jfk-0:~# cli
root@crpd-20-us-jfk-0> show configuration 
## Last commit: 2020-12-23 21:08:40 UTC by root
version 20190606.224121_builder.r1033375;
# Please do not place ssh or netconf service configuration.
# Otherwise, the commit command will be unsuccessful.
# The ssh and netconf service configuration is in the file - /etc/ssh/sshd_config
system {
    root-authentication {
        encrypted-password "$6$258On$US93jLrQPwqjlpgPM84N8f9os9SLB0BgFrsgvcmAcSwrt45DE/GhWpUQTD.pVUgiJbYzxgOkgrrHnbwZfhG.P/"; ## SECRET-DATA
        ssh-rsa "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDBPa5bGj+1dyjJobfcYx8QncJElyykVZGz23QNhPAsNXh4tZl5y6WwJ8oTWHtDzjyydzlgvpujvVnq5b88KPbiotl0DoEslvazhk3O6dl48zojmq/IlMMHI3T0an25soO7wSiSwg1D6xZFL3b9v2Pjseh3axjx0PnshllPv9TvYl12jitNRnoa1WaFIGuOE/rZ5DmAkPSWCh0GTlc35VcxWPqDf/oowBEPxL12Zht/+B22jJ4ZkPoxH+ZGlLUhdOmzqxzA26/V24tvQ+6GHBWfmrjeLVk/J9dr7TAjeFCoLy5dxnaX2yQN4c0vpRwY/1xJcqwb8UbU3TIEXpIANyuR srho@srho-mbp15"; ## SECRET-DATA
    }
    license {
        keys {
            key "20 243 Ni LONG NORMAL STANDALONE AGGR 1_KEYS 1_KEYS 1 DEC 2020 0 0 1 MAY 2021 23 59 NiL SLM_CODE DEMO NiL NiL Ni NiL NiL 15_MINS NiL 0 NHd4thGnai1YWeN5IXXKXoMrcXL68o+vm9d1gv4xwxBenKTdRHMdm8H5q2trWBPboMQY3GvLffAKeUV/uYEa1dMO8dmFlF7JhPnUa9jBGqqJAADQ2QDwF7WRtysXLp/t5XItK0LNCqdGF1A0ZF6UFNs5xeIE+Vqy35MXJB7zmp2mQUM/4HdTl0WRq2nBxTt8Pqp6SKBeRA6/8vnuJaS3L4KOgR+w3ABhfDy0sAo501GSs6RIkX779tTavev3wfWLKcGZm4MthWOPNnS+sP+ku9wGKYqNTG6plc+sNOFHuzPC+44IIotyi8HWGchg7sD+Hods0BowsjGS6NW+p5Sv8X7m+62gAleUQ7EqbcwziGIlA+/f6Ds927ogq9pd51P/thYJ39RtH7OOPfvNnjmrDSOdoNncBt+kQBdCi16ewpBcYl1/g5HdzOkQzHbVOb77PI0Y2rKDq5pAGCw6b3FkDaCF8PPMltozpDYMlwQsKrJG+NSuMwIoMS7BOHLoEQMrkuz7c3XCsiPjwXSrl87NzgLLdENXbDVudqMJSXTHD6DybciYBbYW0dGbNoC/ZN5CBQRSQ0IyC4MJBQRSQ0IyBgEBBwEBBCU4YjFiNmYxZC0zNzY5LTRmNGUtYjM4Ny0zNGUzMmVlOTYzYmQACAEBCUClydIpUZi6n8jzILOrVPag59XNDAizpYck68gc4uLfRITzGmdQTKfa9U1K8BGjzfbX0S4wCDZhm4lh55efpLXVCoIOMIIBCgKCAQEA2OMgESQh1fz4LjXZ0SmDsrPGJ2cB5zU4CPsQmQj0FjnjUYfF41nDGO6O4mpIny5WTzFJYSp61719oZyDEVsJYZwnqK3NzyswNBVj3CqSHx3HpKCu1nqAN1sC79hbxa3LsbvGa2522jtMzhrd7F3MdycGewa2O060rrBz9ZroxirqH6Zx4jmzFRJbgZX7UUOJswW3b5cTKlOyX/YYl1rQIoaZ+f1YtEaIRHQ8j9j8Xn/G4AT1XjQFOH9Yfo39PPERGr1sCGOIlKpYWZJhP6L3HqIMF22tsAWJLVkOIPCuI/PZpk5VMmYZoF3KMdo5kSfATgotW6ufhqSNSbhR6nu7iwIDAQABBCU4YjFiNmYxZC0zNzY5LTRmNGUtYjM4Ny0zNGUzMmVlOTYzYmQABwEBDIIAj/0GXRSG4RZgcziqdmNuGArFJszXA01vxuGTUS1dUjdf0PKBt0rp/92L0SoOPcTT/euVdaFJICecqJh5iqMBAEtLAw2fCeVHWSQOQdyj3dcthKhBU9krhybd9MQ+6Zsi1TUReOKqLiTuum7p6IDyVHqIISAfEhoE7j2A446m1JfJON08LujErm7c2f9PFYI0FMbjocBTuotH2gqaemRPGdqasYEP2aPrOdj/bQeGErw+Y2WULrkPYxQsiLDwTzcnEczDmmRHq0hDvCdCAs2bJ8q0CFezNXWJRSHBqd4ZRWvfg8TCCyKFrFSKqLlH+SAgzqW+D0Njf7kqv87hiVz/QwAAAr8=#KeyType=Commercial#AID=c1cc4da7-2d5a-4020-bca0-4f6940b6f2ab SIGN=0728662D1CF3FEB85EA7F44E569231709B1EC5FC65A9B901D6F41C6603F5D911BB8C58F955AB41DC3624";
        }
    }
}

root@crpd-20-us-jfk-0> show route 

inet.0: 4 destinations, 4 routes (2 active, 0 holddown, 2 hidden)
+ = Active Route, - = Last Active, * = Both

10.128.0.0/20      *[Direct/0] 00:05:25
                    >  via eth0
10.128.0.2/32      *[Local/0] 00:05:25
                       Local via eth0

inet6.0: 3 destinations, 3 routes (2 active, 0 holddown, 1 hidden)
+ = Active Route, - = Last Active, * = Both

fe80::858:aff:fe80:2/128
                   *[Local/0] 00:05:25
                       Local via eth0
ff02::2/128        *[INET6/0] 00:05:25
                       MultiRecv

root@crpd-20-us-jfk-0> 

root@crpd-20-us-jfk-0> show interfaces routing 
Interface        State Addresses
eth0             Up    MPLS  enabled
                       ISO   enabled
                       INET  10.128.0.2
                       INET6 fe80::858:aff:fe80:2
lo.0             Up    MPLS  enabled
                       ISO   enabled
                       INET  127.0.0.1
                       INET6 ::1

```

#### netconf service access
```xml
srho-mbp:crpd srho$ ssh -p 830 root@151.139.31.140
Warning: Permanently added '[151.139.31.140]:830' (ECDSA) to the list of known hosts.
mgd: error: schema: init: could not resolve function 'cos_expand_forwarding_classes'
mgd: error: schema: init: could not resolve function 'cos_expand_forwarding_classes'
<!-- No zombies were killed during the creation of this user interface -->
<!-- user root, class (root) -->
<hello xmlns="urn:ietf:params:xml:ns:netconf:base:1.0">
  <capabilities>
    <capability>urn:ietf:params:netconf:base:1.0</capability>
    <capability>urn:ietf:params:netconf:capability:candidate:1.0</capability>
    <capability>urn:ietf:params:netconf:capability:confirmed-commit:1.0</capability>
    <capability>urn:ietf:params:netconf:capability:validate:1.0</capability>
    <capability>urn:ietf:params:netconf:capability:url:1.0?scheme=http,ftp,file</capability>
    <capability>urn:ietf:params:xml:ns:netconf:base:1.0</capability>
    <capability>urn:ietf:params:xml:ns:netconf:capability:candidate:1.0</capability>
    <capability>urn:ietf:params:xml:ns:netconf:capability:confirmed-commit:1.0</capability>
    <capability>urn:ietf:params:xml:ns:netconf:capability:validate:1.0</capability>
    <capability>urn:ietf:params:xml:ns:netconf:capability:url:1.0?scheme=http,ftp,file</capability>
    <capability>urn:ietf:params:xml:ns:yang:ietf-netconf-monitoring</capability>
    <capability>http://xml.juniper.net/netconf/junos/1.0</capability>
    <capability>http://xml.juniper.net/dmi/system/1.0</capability>
  </capabilities>
  <session-id>152</session-id>
</hello>
]]>]]>


```


#### destroy RPD instance
```bash
srho-mbp:crpd srho$ terraform destroy --auto-approve
stackpath_compute_network_policy.crpd-network-policy: Destroying... [id=2fe73e85-25d1-4af2-b53a-028c629d21ee]
stackpath_compute_network_policy.crpd-network-policy: Destruction complete after 1s
stackpath_compute_workload.crpd: Destroying... [id=43aa4810-5500-4ba5-8e2c-a65119f211d6]
stackpath_compute_workload.crpd: Destruction complete after 0s

Destroy complete! Resources: 2 destroyed.
srho-mbp:crpd srho$ 

```
