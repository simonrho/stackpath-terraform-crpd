#!/bin/bash

cat <<EOL > /etc/ssh/sshd_config
Port 22
PermitRootLogin = yes
Port 830
Subsystem netconf /usr/bin/netconf
Match LocalPort 830
AllowTCPForwarding no
X11Forwarding no
ForceCommand netconf
EOL

service ssh restart

cat <<EOF >> /etc/init.d/rcS
# set loopback ip addresses.
if [ ! -z "${LOOPBACK_ADDRESS}" ]; then
  ip -4 addr add "${LOOPBACK_ADDRESS}/32" dev lo
fi

# commit initial configuration.
/usr/sbin/mgd -d -ZS -ZO -Za autoinstall-merge /juniper.conf || true

EOF
